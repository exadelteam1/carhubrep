package ch.domain.model.enums;


public enum Role {
    ADMIN,
    USER
}
