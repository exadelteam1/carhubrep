/*package ch.domain.model.pkeys;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RouteMemberKey implements Serializable {


    private Long routeID;
    private Long personID;
    private boolean isDriver;

    public RouteMemberKey(Long routeID, Long personID, boolean isDriver) {
        this.routeID = routeID;
        this.personID = personID;
        this.isDriver = isDriver;
    }

    public RouteMemberKey() {

    }

    public Long getRouteID() {
        return routeID;
    }

    public void setRouteID(Long routeID) {
        this.routeID = routeID;
    }

    public Long getPersonID() {
        return personID;
    }

    public void setPersonID(Long personID) {
        this.personID = personID;
    }

    public boolean isDriver() {
        return isDriver;
    }

    public void setDriver(boolean driver) {
        isDriver = driver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RouteMemberKey that = (RouteMemberKey) o;
        return routeID == that.routeID &&
                personID == that.personID &&
                isDriver == that.isDriver;
    }

    @Override
    public int hashCode() {
        return Objects.hash(routeID, personID, isDriver);
    }

}
*/