/*
package ch.domain.model.entity;



import ch.domain.model.pkeys.RouteMemberKey;

import javax.persistence.*;

@Entity
@Table(name = "route_members", schema = "public")
@IdClass(RouteMemberKey.class)
public class RouteMember {


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_route")
    private Route route;

    @Id
    @AttributeOverrides({
            @AttributeOverride(name = "routeID",
                    column = @Column(name = "id_route")),
            @AttributeOverride(name = "personID",
                    column = @Column(name = "id_person")),
            @AttributeOverride(name = "isDriver",
                    column = @Column(name = "is_driver"))
    })
    private Long routeID;
    private Long personID;
    private boolean isDriver;

    @Column(name = "route_len")
    private float routeLen;

    @Column(name = "to_pass_rating")
    private int asPassengerRating;

    @Column(name = "to_driver_rating")
    private int driverRating;

    public RouteMember() {

    }

    public RouteMember(RouteMemberKey key) {
        this.routeID = key.getRouteID();
        this.isDriver = key.isDriver();
        this.personID = key.getPersonID();
    }

    public void setDriverRating(int driverRating) {
        this.driverRating = driverRating;
    }

    public void setRouteLen(int routeLen) {
        this.routeLen = routeLen;
    }

    public void setAsPassengerRating(int asPassengerRating) {
        this.asPassengerRating = asPassengerRating;
    }

    public float getRouteLen() {
        return routeLen;
    }

    public int getAsPassengerRating() {
        return asPassengerRating;
    }

    public int getDriverRating() {
        return driverRating;
    }


}
*/
