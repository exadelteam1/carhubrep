package ch.domain.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(name = "car", schema = "public")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_car")
    private Long id;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name = "id_person")
    private Person owner;

    @Column(name = "color")
    private String color;

    @Column(name = "number")
    private String number;

    @Column(name = "model")
    private String model;

    @Column(name = "is_saved")
    private boolean isSaved;

    public void setId(Long id) {
        this.id = id;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Long getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
}
