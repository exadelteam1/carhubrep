/*
package ch.domain.model.entity;


import javax.persistence.*;
import java.util.Date;
import java.util.List;


*/
/**
 * Simple Java Bean
 *//*


@Entity
@Table(name = "routes_info", schema = "public")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_route")
    private Long id;

    @Column(name = "distance")
    private float distance;

    @Column(name = "from")
    private float[] startPoint;

    @Column(name = "to")
    private float[] endPoint;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "id_car")
    private Long carID;

    @OneToMany(mappedBy = "routes_info", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RouteMember> members;

}
*/
