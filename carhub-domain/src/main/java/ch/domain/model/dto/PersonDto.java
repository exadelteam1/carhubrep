package ch.domain.model.dto;

import ch.domain.model.entity.Car;
import ch.domain.model.enums.Role;

import java.util.List;

public class PersonDto {

    private Long id;
    private String name;
    private String surname;
    private String patronymic;
    private String email;
    private Role role;
    private String phone;
    private String photo;
    private List<Car> cars;
    private Car defaultCar;


    public void setDefaultCar(Car defaultCar) {
        this.defaultCar = defaultCar;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getEmail() {
        return email;
    }

    public Role getRole() {
        return role;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhoto() {
        return photo;
    }

    public List<Car> getCars() {
        return cars;
    }

    public Car getDefaultCar() {
        return defaultCar;
    }
}
