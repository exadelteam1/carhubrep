package ch.converter;


import ch.domain.model.dto.PersonDto;
import ch.domain.model.entity.Person;
import org.springframework.stereotype.Component;

@Component
public class PersonConverter {

    public PersonDto convert(Person p) {
        PersonDto pd = new PersonDto();
        pd.setId(p.getId());
        pd.setEmail(p.getEmail());
        pd.setName(p.getName());
        pd.setPatronymic(p.getPatronymic());
        pd.setSurname(p.getSurname());
        pd.setPhone(p.getPhone());
        pd.setPhoto(p.getPhoto());
        pd.setRole(p.getRole());
        pd.setCars(p.getCars());
        pd.setDefaultCar(p.getDefaultCar());
        return pd;
    }

    public Person convert(PersonDto pd) {
        Person p = new Person();
        p.setCars(pd.getCars());
        p.setDefaultCar(pd.getDefaultCar());
        p.setName(pd.getName());
        p.setSurname(pd.getSurname());
        p.setPatronymic(pd.getPatronymic());
        p.setEmail(pd.getEmail());
        p.setId(pd.getId());
        p.setPhone(pd.getPhone());
        p.setPhoto(pd.getPhoto());
        p.setRole(pd.getRole());
        return p;
    }
}
