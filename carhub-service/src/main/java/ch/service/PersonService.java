package ch.service;

import ch.domain.model.dto.PersonDto;

public interface PersonService {

    PersonDto findById(Long id);
    PersonDto findByEmail(String email);
}
