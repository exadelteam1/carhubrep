package ch.service.impl;

import ch.converter.PersonConverter;
import ch.domain.model.dto.PersonDto;
import ch.domain.model.entity.Person;
import ch.persistence.dao.PersonDao;
import ch.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDao personDao;

    @Autowired
    private PersonConverter personConverter;

    @Override
    public PersonDto findById(Long id) {
        Person p = personDao.findById(id);
        return p == null ? null : personConverter.convert(p);
    }

    @Override
    public PersonDto findByEmail(String email) {
        Person p = personDao.findByEmail(email);
        return p == null ? null : personConverter.convert(p);
    }
}
