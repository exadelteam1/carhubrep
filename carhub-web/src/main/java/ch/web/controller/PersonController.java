package ch.web.controller;


import ch.domain.model.dto.PersonDto;
import ch.domain.model.entity.Person;
import ch.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/person/find/id")
    public PersonDto findById(@RequestParam(value = "id") Long id) {
        return personService.findById(id);
    }

    @GetMapping("/person/find/email")
    public PersonDto findByEmail(@RequestParam(value = "email") String email) {
        return personService.findByEmail(email);
    }
}
