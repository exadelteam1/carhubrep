package ch.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@EnableJpaRepositories(basePackages = {"ch"})
@EntityScan(basePackages = {"ch"})
@ComponentScan(basePackages = {"ch"})
public class DemoApplication {


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        /*Car car = new Car();
        Person person = new Person();
        person.setName("Sadio");
        person.setSurname("Mane");
        person.setPatronymic("123");
        person.setEmail("123");
        person.setPassword("123");
        person.setPhone("123");
        person.setPhoto("123");
        person.setRole(Role.ADMIN);
        car.setColor("red");
        car.setModel("12310");
        car.setSaved(true);
        car.setNumber("11");
        car.setOwner(person);
        CarDao carDao = new CarDaoImpl();
        carDao.save(car);*/
    }
}
