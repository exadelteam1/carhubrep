package ch.persistence.dao;


import ch.domain.model.entity.Person;
import org.springframework.stereotype.Repository;


@Repository
public interface PersonDao  {
    Person findById(Long id);
    Person findByEmail(String email);
}

